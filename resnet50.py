#! /usr/bin/env nix-shell
#! nix-shell default.nix -i python

from keras.applications.resnet50 import ResNet50
from keras.preprocessing import image
from keras.applications.resnet50 import preprocess_input, decode_predictions
import numpy as np
from keras import backend as K

model = ResNet50(weights='imagenet')

img_path = 'giraffe.jpg'
img = image.load_img(img_path, target_size=(224, 224))
x = image.img_to_array(img)
x = np.expand_dims(x, axis=0)
x = preprocess_input(x)

# Simple prediction
# preds = model.predict(x)
# print('Predicted:', decode_predictions(preds, top=3)[0])

# Dump layers
# model.summary()

# Dump output of all layers
inp = model.input
outputs = [layer.output for layer in model.layers]
functor = K.function([inp] + [K.learning_phase()], outputs)
layer_outs = functor([x, 1.])
print(layer_outs)
