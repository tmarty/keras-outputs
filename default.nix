with import <nixpkgs> {};
stdenv.mkDerivation rec {
  name = "keras-cnn";
  buildInputs = [
    theano
  ] ++ (with python36Packages; [
    Keras
    Theano
    h5py
    pillow
  ]);
}
