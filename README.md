Simple keras test: dump all ResNet50 intermediate layers output on an image inference.

 * Test with Nix:
   * run `./resnet50.py`  
 * Test without Nix:
   * install all dependencies
   * run `python resnet50.py`

Code is based on:
 * https://keras.io/applications/#resnet50
 * https://stackoverflow.com/questions/41711190/keras-how-to-get-the-output-of-each-layer  

Model source:
 * https://github.com/fchollet/deep-learning-models/blob/master/resnet50.py
